export class ConquestResult  {
    squadra1 : string;
    squadra2 : string;
    armate1 : number;
    armate2 : number;
    finalArmate1 : number;
    finalArmate2 : number;
    livello1 : number;
    livello2 : number;
}